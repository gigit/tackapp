-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2014 at 08:48 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tackapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `kaskus`
--

CREATE TABLE IF NOT EXISTS `kaskus` (
  `id` bigint(20) unsigned NOT NULL,
  `import` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `olx`
--

CREATE TABLE IF NOT EXISTS `olx` (
  `id` bigint(20) unsigned NOT NULL,
  `import` text NOT NULL,
  `export` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tokopedia`
--

CREATE TABLE IF NOT EXISTS `tokopedia` (
  `id` bigint(20) unsigned NOT NULL,
  `import` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tokopedia`
--

INSERT INTO `tokopedia` (`id`, `import`) VALUES
(1, '[{"name":"Minion Wireless Mouse A73","url":"https:\\/\\/www.tokopedia.com\\/darurat\\/minion-wireless-mouse-a73","photo":["https:\\/\\/ecs3.tokopedia.net\\/newimg\\/product-1\\/2014\\/11\\/25\\/265039\\/265039_2710d592-746f-11e4-a21c-bc892523fab8.jpg","https:\\/\\/ecs3.tokopedia.net\\/newimg\\/product-1\\/2014\\/11\\/25\\/265039\\/265039_278139a4-746f-11e4-a21c-bc892523fab8.jpg"],"description":"New Model","price":"400000"},{"name":"Minion Mouse Wireless - A347","url":"https:\\/\\/www.tokopedia.com\\/darurat\\/minion-mouse-wireless-a347","photo":["https:\\/\\/ecs3.tokopedia.net\\/newimg\\/product-1\\/2014\\/11\\/25\\/265039\\/265039_aef0dd1e-746e-11e4-a4ea-daf14908a8c2.jpg","https:\\/\\/ecs3.tokopedia.net\\/newimg\\/product-1\\/2014\\/11\\/25\\/265039\\/265039_af12959e-746e-11e4-a4ea-daf14908a8c2.jpg"],"description":"Bagus bro","price":"200000"},{"name":"Minion Laptop X","url":"https:\\/\\/www.tokopedia.com\\/darurat\\/minion-laptop-x","photo":["https:\\/\\/ecs3.tokopedia.net\\/newimg\\/product-1\\/2014\\/11\\/24\\/265039\\/265039_8e2b3500-73f8-11e4-939d-6b9c2523fab8.jpg"],"description":"Core new x4","price":"10000000"}]');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `import` varchar(500) NOT NULL DEFAULT '{}',
  `export` varchar(500) NOT NULL DEFAULT '{}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `import`, `export`) VALUES
(1, 'thor@mailinator.com', '{"olx":"daruratbro","kaskus":"Electra14","tokopedia":"darurat"}', '{"olx":{"username":"daruratm699@gmail.com","password":"rahasiajo"}}');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kaskus`
--
ALTER TABLE `kaskus`
  ADD CONSTRAINT `kaskus_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `olx`
--
ALTER TABLE `olx`
  ADD CONSTRAINT `olx_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tokopedia`
--
ALTER TABLE `tokopedia`
  ADD CONSTRAINT `tokopedia_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
