<!doctype html>
<html>
<head>
	@include('admin.includes.head')
	
	@yield('css')

</head>
<body>
	<section id="container">

		<!--header start-->
		<header class="header white-bg">
			@include('admin.includes.header')
		</header>
		<!--header end-->

		<!--sidebar start-->
		<aside>
			@include('admin.includes.sidebar')
		</aside>
		<!--sidebar end-->

		<!--main content start-->
		<section id="main-content">
			<section class="wrapper site-min-height">
				@yield('content')
			</section>
		</section>
		<!--main content end-->

		<!--footer start-->
		<footer class="site-footer">
			@include('admin.includes.footer')
		</footer>
		<!--footer end-->

	</section>

	@include('admin.includes.foot')

  @yield('js')

</body>
</html>