<!doctype html>
<html>
<head>
	@include('admin.includes.head')
	
	@yield('css')

</head>
<body class="full-width login-body">
	<div class="login-wrapper"></div>
	<section id="container">

		<!--header start-->
		<!--header end-->

		<!--sidebar start-->

		<!--sidebar end-->

		<!--main content start-->
		<section id="main-content">
			<section class="wrapper login-min-height" id="full-login">
				@yield('content')
			</section>
		</section>
		<!--main content end-->

		<!--footer start-->

		<!--footer end-->

	</section>

	@include('admin.includes.foot')

  @yield('js')

</body>
</html>