@extends('admin.layouts.default')

@section('css')
<!-- CSS -->

@stop

@section('content')
<!-- CONTENT -->
<div class="col-lg-12">
	<section class="panel">
		<header class="panel-heading" style="border-left: 4px solid #FF6C60;">
			<img src="{{URL::to('/assets/kaskus.jpg')}}" height="50px">
			<div class="font-heading" style="margin-top: 15px;">Import from Kaskus</div>
		</header>
	</section>
	<section class="panel">
		<header class="panel-heading">
			User Setting
		</header>
		<div class="panel-body">
			<form method="POST" class="form-horizontal" role="form">
				<div class="form-group">
					<label for="inputFaculty" class="col-lg-2 col-sm-2 control-label">Username</label>
					<div class="col-lg-10">
						<input name="username" type="text" class="form-control" id="" value="Electra14">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button class="btn btn-danger" type="submit">Save</button>
					</div>
				</div>
			</form>
		</div>
	</section>
</div>
<div class="col-lg-12">
	<section class="panel">
		<header class="panel-heading">
			Import
		</header>
		<div class="panel-body">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th><i class="fa fa-th-large"></i> Product</th>
						<th class="hidden-phone"><i class="fa fa-info-circle"></i> Description</th>
						<th><i class="fa fa-money"></i> Price</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php for ($i=0; $i < count($iklan); $i++): ?>
					<?php
					$linkIklan = 'www.kaskus.com'.$iklan[$i][1];
					$detailIklan = KaskusLib::getDetailIklan($linkIklan);
					?>
					<tr>
						<td>
							<img src="{{ $detailIklan['gambar'][0] }}">
							<br>
							<h5 class="text-product">{{ $iklan[$i][0] }}</h5>
						</td>
						<td class="hidden-phone">{{ $detailIklan['deskripsi'] }}</td>
						<td>Rp. {{ $detailIklan['harga'] }}</td>
						<td>
							<form id="form{{$i}}" method="POST">
								<input type="hidden" name="product_name" value="{{ $iklan[$i][0] }}">
								<input type="hidden" name="description" value="{{ $detailIklan['deskripsi'] }}">
								<input type="hidden" name="price" value="{{ $detailIklan['harga'] }}">
								<?php $number = 0;?>
								@foreach ($detailIklan['gambar'] as $key2 => $value2)
								<input type="hidden" name="images[{{$number++}}][url]" value="{{$value2}}">
								@endforeach
								<a href="#_" onclick="imp('form{{$i}}')" class="btn btn-success"><i class="fa fa-download"></i> Import</a>
							</form>
						</td>
					</tr>
				<?php endfor ?>
			</tbody>
		</table>
	</div>
</section>
</div>
@stop

@section('js')
<!-- JS -->
<script>
function imp(frm){
	var icon = $('#'+frm).children().last().children();
	icon.removeClass('fa-download').addClass('fa-spinner fa-spin');
	$.post("{{URL::to('test/import')}}",$('#'+frm).serialize())
	.done(function(res){
		toastr.success('Product Imported !!');
		icon.parent().html('<i class="fa fa-download"></i> Import');
	});
}
</script>
@stop