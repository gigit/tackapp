@extends('admin.layouts.default')

@section('css')
<!-- CSS -->

@stop

@section('content')
<!-- CONTENT -->
<div class="col-lg-12">
	<section class="panel">
		<header class="panel-heading" style="border-top: 4px solid #009688; width:100%;">
			<img src="{{URL::to('/assets/olx.jpg')}}" height="50px">
			<div class="font-heading" style="margin-top: 15px;">Export Product to OLX</div>
		</header>
	</section>
	<section class="panel">
		<header class="panel-heading panel-heading-custom" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			<div class="col-md-6 panel-heading-title">
				User Setting &nbsp
			</div>
			<div class="col-md-2 col-md-offset-4">
				<p style="text-align:right"><i class="fa fa-sort-down"></i></p>
			</div>
		</header>
		<div class="panel-body collapse" id="collapseOne" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
			<form method="POST" class="form-horizontal" action="{{URL::to('user-export/olx')}}" role="form">
				<div class="form-group">
					<label for="inputFaculty" class="col-lg-2 col-sm-2 control-label">Username</label>
					<div class="col-lg-10">
						<input name="username" type="text" class="form-control" id="" value="{{{$user->username or ''}}}">
					</div>
				</div>
				<div class="form-group">
					<label for="inputFaculty" class="col-lg-2 col-sm-2 control-label">Password</label>
					<div class="col-lg-10">
						<input name="password" type="password" class="form-control" value="{{{$user->password or ''}}}" >
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button class="btn btn-shadow btn-danger" type="submit">Save</button>
					</div>
				</div>
			</form>
			<hr>
			<!-- <form method="POST" class="form-horizontal" role="form">
				<div class="form-group">
					<label for="inputFaculty" class="col-lg-2 col-sm-2 control-label">Login to Facebook</label>
					<div class="col-lg-10">
						<button class="btn btn-shadow btn-primary" type="button" id="fb-button"><i class="fa fa-facebook"></i> Connect to Facebook</button>
					</div>
				</div>
			</form> -->
		</div>
	</section>
</div>
<div class="col-lg-12">
	@if($products->total>0)
	<section class="panel">
		<header class="panel-heading panel-main-custom">
			Export
		</header>
		<div class="panel-body">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th><input id="chall" type="checkbox"></th>
						<th><i class="fa fa-th-large"></i> Product</th>
						<th><i class="fa fa-tag"></i> Name</th>
						<th class="hidden-phone"><i class="fa fa-info-circle"></i> Description</th>
						<th><i class="fa fa-money"></i> Price</th>
						<th><a href="#_" onclick="acua()" class="btn btn-success"><i class="fa fa-upload"></i> Export Selected</a></th>
					</tr>
				</thead>
				<tbody>
					@for ($i=0; $i < $products->total; $i++)
					<tr>
						<td><input id="ch{{$i}}" type="checkbox" class="ch" value="{{$i}}"></td>
						<td>
							<img src="{{$products->data[$i]->images[0]->url or URL::to('assets/no-img.jpg')}}"  style="max-width: 300px;">
						</td>
						<td><h5 class="text-product">{{$products->data[$i]->name}}</h5></td>
						<td class="hidden-phone">{{$products->data[$i]->description}}</td>
						<td>Rp. {{$products->data[$i]->price}}</td>
						<td>
							<form id="form{{$i}}" method="POST">
								<input type="hidden" name="product_id" value="{{$products->data[$i]->id}}">
								<input type="hidden" name="image" id="image" value="{{$products->data[$i]->images[0]->url}}">
								<input type="hidden" name="name" id="name" value="{{$products->data[$i]->name}}">
								<input type="hidden" name="price" id="price" value="{{$products->data[$i]->price}}">
								<input type="hidden" name="desc" id="desc" value="{{$products->data[$i]->description}}">
								<!-- <button type="button" onclick="formNum('form{{$i}}')" class="btn btn-success" data-toggle="modal" data-target="#export"><i class="fa fa-upload"></i> Export</button> -->
								<button type="button" href="#_" onclick="exp('form{{$i}}')" class="btn btn-success" ><i class="fa fa-upload"></i> Export</button>
							</form>
						</td>
					</tr>
					@endfor
				</tbody>
			</table>
		</div>
	</section>
	@else
	<div class="alert alert-block alert-danger fade in">
		<button data-dismiss="alert" class="close close-sm" type="button">
			<i class="fa fa-times"></i>
		</button>
		<strong>No products!</strong> Please add product to your TackThis first!
	</div>
	@endif
</div>

<!-- Modal -->
<div class="modal fade" id="export" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Export To</h4>
			</div>
			<div class="modal-body">
				<h3>Online Market</h3>
				<hr>
				<input type="checkbox" checked> <img src="{{URL::to('/assets/olx.jpg')}}" height="30px">
				<br>
				<br>
				<input type="checkbox" disabled> <img src="{{URL::to('/assets/kaskus.jpg')}}" height="30px">
				<br>
				<br>
				<input type="checkbox" disabled> <img src="{{URL::to('/assets/tp.png')}}" height="30px">
				<br>
				<br>
				<h3>Social Media</h3>
				<hr>
				<input type="checkbox" checked> <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-facebook"></i> Facebook</button>
				<br>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" id="expButton" onclick="exp()" class="btn btn-primary"><i class="fa fa-upload"></i> Export</button>
			</div>
		</div>
	</div>
</div>
<!-- <div id="fb-root"></div> -->
@stop

@section('js')
<!-- JS -->
<script>
function exp(frm) {
	var icon = $('#'+frm).children().last().children();
	icon.removeClass('fa-upload').addClass('fa-spinner fa-spin');
	$.post("{{URL::to('olx/export')}}",$('#'+frm).serialize())
	.done(function(res){
		//error login 
		if(res=='3'){
			toastr.error('Login failed!');
		}else{
			toastr.success('Product Exported !!');
		}
		icon.parent().html('<i class="fa fa-upload"></i> Export');
	});
}
$('#chall').change(function(){
	if($(this).is(':checked')){
		$('.ch').prop('checked',true);
	}else{
		$('.ch').prop('checked',false);
	}
});
function acua(){
	$('.ch').each(function(){
		if($(this).is(':checked')){
			console.log($(this).val());
			exp('form'+$(this).val())
		}
	});
}

//fb

// function exp() {
// 	var name = $('#name').val();
// 	var price = $('#price').val();
// 	var desc = $('#desc').val();
// 	var img = $('#image').val();
// 	share(img, name, price, desc);


// window.fbAsyncInit = function() {
// 	FB.init({
// 		appId      : '1525618994361237',
// 		xfbml      : true,
// 		version    : 'v2.2',
// 		status     : true,
// 	});
// };

// (function(d, s, id){
// 	var js, fjs = d.getElementsByTagName(s)[0];
// 	if (d.getElementById(id)) {return;}
// 	js = d.createElement(s); js.id = id;
// 	js.src = "//connect.facebook.net/en_US/sdk.js";
// 	fjs.parentNode.insertBefore(js, fjs);
// }(document, 'script', 'facebook-jssdk'));

// $(document).ready(function() {
// 	$('#fb-button').click(function() {
// 		login(function() {
// 			console.log('login');
// 		})
// 	});
// });


// function login(callback) {
// 	console.log('connecting');
// 	FB.login(function(response) {
// 		if (response.authResponse) {
// 			console.log('Welcome!  Fetching your information.... ');
// 			FB.api('/me', function(response) {
// 				var name = response.name;
// 				// console.log('Good to see you, ' + response.name + '.');
// 				$('#fb-button').html('<i class="fa fa-facebook"></i> ' + response.name);
// 				status(response);
// 			});
// 		} else {
// 			console.log('User cancelled login or did not fully authorize.');
// 		}
// 	}, {
// 		scope: 'publish_actions', 
// 		return_scopes: true
// 	});
// }

// function status(){
// 	FB.getLoginStatus(function(response) {
// 		if (response.status === 'connected') {
// 			console.log('connected');
// 			var uid = response.authResponse.userID;
// 			var accessToken = response.authResponse.accessToken;
// 			console.log(accessToken);
// 		} else if (response.status === 'not_authorized') {
// 			console.log('not_authorized');
// 		} else {
// 			console.log('not_login');
// 		}
// 	});
// }

// // function share(url) {
// function share(url, product_name, price, desc) {
// 	FB.api(
// 		"/me/photos",
// 		"POST",
// 		{
// 			"message" : product_name + "\nPrice : " + price + "\nDescription : " + desc,
// 			"url": url
// 		},
// 		function (response) {
// 			if (response && !response.error) {
// 				console.log(response);
// 			}
// 		}
// 		);
// }

</script>
@stop