@extends('admin.layouts.default')

@section('css')
<!-- CSS -->

@stop

@section('content')
<!-- CONTENT -->
<div class="col-lg-12">
	<section class="panel">
		<header class="panel-heading" style="border-top: 4px solid #009688; width:100%;">
			<img src="{{URL::to('/assets/kaskus.jpg')}}" height="50px">
			<div class="font-heading" style="margin-top: 15px;">Import from Kaskus</div>
		</header>
	</section>
	<section class="panel">
		<header class="panel-heading panel-heading-custom" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			<div class="col-md-6 panel-heading-title">
				User Setting &nbsp
			</div>
			<div class="col-md-2 col-md-offset-4">
				<p style="text-align:right"><i class="fa fa-sort-down"></i></p>
			</div>
		</header>
		<div class="panel-body collapse" id="collapseOne" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
			<form method="POST" class="form-horizontal" action="{{URL::to('user-import/olx')}}" role="form">
				<div class="form-group">
					<label for="inputFaculty" class="col-lg-2 col-sm-2 control-label">Username</label>
					<div class="col-lg-10">
						<input name="username" type="text" class="form-control" id="" value="{{{$user or ''}}}">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button class="btn btn-danger" type="submit">Save</button>
					</div>
				</div>
			</form>
		</div>
	</section>
</div>
<div class="col-lg-12">
	@if(!empty($products))
	<section class="panel">
		<header class="panel-heading panel-main-custom">
			Import
		</header>
		<div class="panel-body">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th><input id="chall" type="checkbox"></th>
						<th><i class="fa fa-th-large"></i> Product</th>
						<th><i class="fa fa-tag"></i> Name</th>
						<th class="hidden-phone"><i class="fa fa-info-circle"></i> Description</th>
						<th><i class="fa fa-money"></i> Price</th>
						<th><a href="#_" onclick="acua()" class="btn btn-success"><i class="fa fa-download"></i> Import Selected</a></th>
					</tr>
				</thead>
				<tbody>
					@foreach($products as $key => $product)
					<tr>
						<td><input id="ch{{$key}}" type="checkbox" class="ch" value="{{$key}}"></td>
						<td>
							<img src="{{ $product->detail->images[0] }}"  style="max-width: 300px;">
						</td>
						<td>
							<h5 class="text-product">{{ $product->detail->name }}</h5>
						</td>
						<td class="hidden-phone">{{ $product->detail->desc }}</td>
						<td>Rp. {{ $product->detail->price }}</td>
						<td>
							<form id="form{{ $key }}" method="POST">
								<input type="hidden" name="product_name" value="{{ $product->detail->name }}">
								<input type="hidden" name="description" value="{{ $product->detail->desc }}">
								<input type="hidden" name="price" value="{{ $product->detail->price }}">
								@foreach ($product->detail->images as $key2 => $image)
								<input type="hidden" name="images[{{ $key2 }}][url]" value="{{ $image }}">
								@endforeach
								<a href="#_" onclick="imp('form{{ $key }}')" class="btn btn-success"><i class="fa fa-download"></i> Import</a>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</section>
	@else
	@if(empty($user))
	<div class="alert alert-block alert-danger fade in">
		<button data-dismiss="alert" class="close close-sm" type="button">
			<i class="fa fa-times"></i>
		</button>
		<strong>Failed to get product!</strong> Please setup the username in user setting!
	</div>
	@else
	<div class="alert alert-block alert-danger fade in">
		<button data-dismiss="alert" class="close close-sm" type="button">
			<i class="fa fa-times"></i>
		</button>
		<strong>No products!</strong> Try to reload again! if it still have no products please check your username!
	</div>
	@endif
	@endif
</div>
@stop

@section('js')
<!-- JS -->
<script>
function imp(frm){
	var icon = $('#'+frm).children().last().children();
	icon.removeClass('fa-check').addClass('fa-spinner fa-spin');
	$.post("{{URL::to('kaskus/import')}}",$('#'+frm).serialize())
	.done(function(res){
		toastr.success('Product Imported !!');
		icon.parent().addClass('disabled').html('<i class="fa fa-check"></i> Imported');
	});
}
$('#chall').change(function(){
	if($(this).is(':checked')){
		$('.ch').prop('checked',true);
	}else{
		$('.ch').prop('checked',false);
	}
});
function acua(){
	$('.ch').each(function(){
		if($(this).is(':checked')){
			console.log($(this).val());
			imp('form'+$(this).val())
		}
	});
}

</script>
@stop