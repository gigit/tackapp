@extends('admin.layouts.default')

@section('css')
<!-- CSS -->

@stop

@section('content')
<!-- CONTENT -->
<div class="col-lg-12">
	<section class="panel">
		<header class="panel-heading">
			Import
		</header>
		<div class="panel-body">
			<table class="table table-striped table-advance table-hover">
				<thead>
					<tr>
						<th><i class="fa fa-bullhorn"></i> Product</th>
						<th class="hidden-phone"><i class="fa fa-question-circle"></i> Description</th>
						<th><i class="fa fa-bookmark"></i> Price</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><a href="#">Vector Ltd</a></td>
						<td class="hidden-phone">Lorem Ipsum dorolo imit</td>
						<td>Rp. 50000</td>
						<td>
							<button class="btn btn-success btn-xs"><i class="fa fa-check"></i> Import</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</section>
</div>
@stop

@section('js')
<!-- JS -->
<script>

</script>
@stop