@extends('admin.layouts.login')

@section('css')
<!-- CSS -->

@stop

@section('content')
<!-- CONTENT -->

<div class="container">
	<div class="logo">
		<img src="{{ URL::to('/assets/logo.png')}}" style="">
	</div>
	<div class="logo-text">
		<h4>Add, Choose, Relax</h4>
	</div>

	{{ Form::open(array('class' => 'form-login','url' => '/auth-login')) }}
	<h2 class="form-login-heading">sign in now</h2>
	<div class="login-wrap">
		<input type="text" name="email" class="form-control" placeholder="Email" autofocus>
		<input type="password" name="password" class="form-control" placeholder="Password">
		<label class="checkbox">
			<input type="checkbox" value="remember-me"> Remember me
		</label>
		<button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
		<div class="registration">
			Don't have TackThis account yet?
			<a class="" href="http://dashboard.tackthis.com/">
				Create an Tackthis account
			</a>
		</div>

	</div>
	{{ Form::close() }}

</div>

@stop

@section('js')
<!-- JS -->

@stop