<div id="sidebar"  class="nav-collapse ">
  <!-- sidebar menu start-->
  <ul class="sidebar-menu" id="nav-accordion">
    <li>
      <a class="" href="{{URL::to('/')}}">
        <i class="fa fa-dashboard"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="sub-menu dcjq-parent-li">
      <a href="javascript:;" class="dcjq-parent">
        <i class="fa fa-download"></i>
        <span>Import</span>
        <span class="dcjq-icon"></span></a>
        <ul class="sub" style="overflow: hidden; display: block;">
          <li><a href="{{URL::to('olx/import')}}">olx</a></li>
          <li><a href="{{URL::to('kaskus/import')}}">kaskus</a></li>
          <li><a href="{{URL::to('tokopedia/import')}}">tokopedia</a></li>
        </ul>
      </li>
    <li>
      <li>
      <a href="{{URL::to('/export')}}">
        <i class="fa fa-upload"></i>
        <span>Export</span>
      </a> 
  </ul>
  <!-- sidebar menu end-->
</div>