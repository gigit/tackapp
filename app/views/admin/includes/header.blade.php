<div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
    </div>
    <!--logo start-->
    <a href="{{URL::to('admin')}}" class="logo">Tack<span>app</span></a>
    <!--logo end-->

    <div class="top-nav ">
            <!--search & user info start-->
        <ul class="nav pull-right top-menu">
            <li>
                <input type="text" class="form-control search" placeholder="Search">
            </li>
                <!-- user login dropdown start-->
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="username">{{ TackthisLib::user()->first_name.' '.TackthisLib::user()->last_name }}</span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <div class="log-arrow-up"></div>
                    
                    <li><a href="{{URL::to('logout')}}"><i class="fa fa-key"></i> Logout</a></li>
                </ul>
            </li>
                <!-- user login dropdown end -->
        </ul>
            <!--search & user info end-->
    </div>
