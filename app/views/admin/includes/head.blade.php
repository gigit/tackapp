    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Januaripin">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="{{URL::to('themes/img/favicon.png')}}">

    <title>Tackapp</title>

    <!-- Bootstrap core CSS -->
    <link href="{{URL::to('themes/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('themes/css/bootstrap-reset.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{URL::to('themes/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <link href="{{URL::to('themes/css/toastr.min.css')}}" rel="stylesheet" />
    <!-- <link href="{{URL::to('themes/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css')}}" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="{{URL::to('themes/css/owl.carousel.css')}}" type="text/css"> -->
    <!-- Custom styles for this template -->
    <link href="{{URL::to('themes/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::to('themes/css/style-responsive.css')}}" rel="stylesheet" />
    <link href="{{URL::to('themes/custom/css/custom.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="{{URL::to('themes/js/html5shiv.js')}}"></script>
      <script src="{{URL::to('themes/js/respond.min.js')}}"></script>
      <![endif]-->