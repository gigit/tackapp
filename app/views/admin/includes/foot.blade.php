  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{{URL::to('themes/js/jquery.js')}}"></script>
  <script src="{{URL::to('themes/js/jquery-1.8.3.min.js')}}"></script>
  <script src="{{URL::to('themes/js/bootstrap.min.js')}}"></script>
  <script class="include" type="text/javascript" src="{{URL::to('themes/js/jquery.dcjqaccordion.2.7.js')}}"></script>
  <script src="{{URL::to('themes/js/jquery.scrollTo.min.js')}}"></script>
  <script src="{{URL::to('themes/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
  <script src="{{URL::to('themes/js/jquery.sparkline.js')}}" type="text/javascript"></script>
  <script src="{{URL::to('themes/js/respond.min.js')}}" ></script>
  <script src="{{URL::to('themes/js/toastr.min.js')}}"></script>
  <!--common script for all pages-->
  <script src="{{URL::to('themes/js/common-scripts.js')}}"></script>
  {{--Menampilkan pesan dari suatu controller dengan session flash--}}
  {{--Menampilkan pesan dari suatu controller dengan session flash--}}

  <script type="text/javascript">
  @if (Session::has('message'))
  toastr.error('{{Session::get('message')}}');
  @endif
  </script>
