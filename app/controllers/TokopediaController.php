<?php 
class TokopediaController extends BaseController
{
	public function getImport()
	{
		$user 	 	= User::find(Session::get('user_id'));
		$product 	= '';
		$marketUser = '';
		$importUser = json_decode($user->import);
		if(!empty($importUser->tokopedia)){
			$marketUser = $importUser->tokopedia;
			$product 	= Tokopedia::find(Session::get('user_id'));
			if(!empty($product->import))$product 	= json_decode($product->import);
		}
		return View::make('admin.pages.import-tokopedia')
		->with('product',$product)
		->with('user',$marketUser);
	}

	public function getSync(){
		$user 	 	= User::find(Session::get('user_id'));
		$importUser = json_decode($user->import);
		if(empty($importUser->tokopedia)){
			echo 'no user';
			die();
		}
		$marketUser = $importUser->tokopedia;

		$url 	= "https://www.tokopedia.com/".$marketUser;
		
		$result = Curl::set()->startCurl($url,'get','',true);

		$result  = Curl::set()->match($result, 'class="grid-shop-product', 'class="pagination');
		$products = Curl::set()->matchAll($result, '<a href="', '"');
		
		if(empty($products)){
			echo 'empty product';
			die();
		}

		foreach ($products as $key => $value) {
			$result 	= Curl::set()->startCurl($value,'get','',true);

			//get photos
			$outerPhotos= Curl::set()->match($result, '<div class="jcarousel product-imagethumb-alt">', '</div>');
			$photos 	= Curl::set()->matchAll($outerPhotos, '<a href="', '"');

			//get deskripsi
			$description  = Curl::set()->match($result, 'itemprop="description" class="mt-20">', '</p>');

			//get harga
			$price		= Curl::set()->match($result, 'itemprop="price">Rp ', ',-');
			$price 		= str_replace('.', '',$price);

			//get nama 
			$name		= Curl::set()->match($result, 'itemprop="name">', '</');

			//insert
			$products[$key] = array(
				"name" 	=> $name,
				"url" 	=> $value,
				"photo"	=> $photos,
				"description"  => $description,
				"price" => $price
				);
		}

		$obj = Tokopedia::firstOrCreate(array('id'=>Session::get('user_id')));
		$obj->import = json_encode($products);
		$res = $obj->save();
		echo $res;
	}

	public function postImport(){
		$data = Input::All();
		if(!Tackthis::auth()){
			Tackthis::login('thor@mailinator.com', 'rahasiajo');
		}
		echo json_encode($data);
		Tackthis::postProduk($data);
		echo '1';
	}

	public function getExport()
	{

		$url = "https://www.tokopedia.com/login.pl";

		//do login

		// $fields = array('email' => 'thorxyz@mailinator.com','pwd' => 'rahasiajo' );

		// $result = Curl::set()->startCurl($url,'post',$fields,true);
		// $result = Curl::set()->startCurl("https://www.tokopedia.com/",'get','',true);
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
		$cookie_file_path = dirname(__FILE__)."/cookie.txt";

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
		curl_close ($ch);



		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array("email=thorxyz%40mailinator.com&pwd=rahasiajo"));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded','Cache-Control: no-cache'));
		curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIE, '__sonar=11914190595124514448; _gat=1; l=0; _ga=GA1.2.1052578432.1416754598; __asc=afbe654e149ea10906a358ec90d; __auc=ee1a87e4149dd28da04fbc7bd4a; _tks=0eudri7rbcjrjt14gftiiv8sqpxrb4i7' );
		$server_output = curl_exec ($ch);
		$http_code = curl_getinfo($ch);
		curl_close ($ch);
		print_r($http_code);
		echo $server_output;


		// return View::make('admin.pages.export-kaskus');
	}



	function microtime_float()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
}