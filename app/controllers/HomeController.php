<?php 
class HomeController extends BaseController
{
	public function getIndex(){
		if (TackthisLib::auth()->check()) {
			return Redirect::to('/home');
		} else {
			return View::make('admin.pages.login');
		}
	}

	public function getHome(){
		if (TackthisLib::auth()->check()) {
			return View::make('admin.pages.home');
		} else {
			return Redirect::to('/');
		}
	}

	public function getLogout(){
		TackthisLib::auth()->logout();
		$session_id = Session::getId();
		File::delete(storage_path().'\cookie'.$session_id.'.txt');
		Session::clear();
		return Redirect::to('/');
	}

	public function postAuthLogin(){
		$data = Input::all();
		if (TackthisLib::auth()->login($data['email'], $data['password'])) {
			// Session::flash('message','Welcome!');
			return Redirect::to('/home');
		} else {
			// Session::flash('message','Failed, username/password false');
			return  View::make('admin.pages.login');
		}
	}
	
	public function getExport(){
		$user 	 	= User::find(Session::get('user_id'));
		$products 	= '';
		$marketUser = '';
		$exportUser = json_decode($user->export);
		if(!empty($exportUser->olx)){
			$marketUser = $exportUser->olx;
		}
		
		$products = json_decode(TackthisLib::auth()->getAllProducts());
		return View::make('admin.pages.export-olx')
		->with('products',$products)
		->with('user',$marketUser);
	}

	public function postUserImport($market){
		$user   = User::find(Session::get('user_id'));
		$import = (array) json_decode($user->import);
		$import[$market] = Input::get('username');
		$user->update(array('import'=>json_encode($import)));
		return Redirect::to($market.'/import');
	}

	public function postUserExport($market){
		$user   = User::find(Session::get('user_id'));
		$export = (array) json_decode($user->export);
		$export[$market] = Input::all();
		$user->update(array('export'=>json_encode($export)));
		return Redirect::to('/export');
	}
}