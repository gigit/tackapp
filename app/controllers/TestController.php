<?php 
class TestController extends BaseController
{

	public function getIndex()
	{
		return View::make('admin.pages.testpage');
	}

	public function getPost()
	{
		$id_user = KaskusLib::searchUser("electra14");
		$iklan = KaskusLib::getIklan($id_user);
		return View::make('admin.pages.test-post')->withIklan($iklan);
	}

	public function postImport(){
		$data = Input::All();
		TackthisLib::make()->postProduct($data);
	}

}