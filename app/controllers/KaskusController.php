<?php 
class KaskusController extends BaseController
{
	public function getImport()
	{
		$user 	 	= User::find(Session::get('user_id'));
		$product 	= '';
		$marketUser = '';
		$importUser = json_decode($user->import);
		if(!empty($importUser->kaskus)){
			$marketUser = $importUser->kaskus;
			$kaskus_user_id = KaskusLibNew::searchUser($marketUser);
			$product = KaskusLibNew::getProducts($kaskus_user_id);
		}

		return View::make('admin.pages.import-kaskus')
		->withProducts($product)
		->withUser($marketUser);
	}

	public function postImport(){
		$data = Input::All();
		TackthisLib::postProduct($data);
	}

	public function getExport()
	{
		return View::make('admin.pages.export-kaskus');
	}

}