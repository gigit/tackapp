<?php 
class OlxController extends BaseController
{
	public function getImport(){
		$user 	 	= User::find(Session::get('user_id'));
		$product 	= '';
		$marketUser = '';
		$importUser = json_decode($user->import);
		if(!empty($importUser->olx)){
			$marketUser = $importUser->olx;
			$product = OlxLib::getProducts($marketUser);
		}

		return View::make('admin.pages.import-olx')
		->with('product',$product)
		->with('user',$marketUser);
	}

	public function postImport(){
		$data = Input::All();
		TackthisLib::postProduct($data);
		echo '1';
	}

	public function getExport(){
		$allProducts = json_decode(TackthisLib::getAllProducts());
		return View::make('admin.pages.export-olx')->withProducts($allProducts);
	}

	public function postExport(){
		$data = Input::all();
		$user 	 	= User::find(Session::get('user_id'));
		$product 	= '';
		$marketUser = '';
		$exportUser = json_decode($user->export);
		if(!empty($exportUser->olx)){
			$marketUser = $exportUser->olx;
		}

		$res  = OlxLib::auth()->login($marketUser->username, $marketUser->password);
		if($res==3){
			return 3;
		}else{
			OlxLib::postProduct($data['product_id']);
		}
	}

	public function getCoba(){
		echo "coba";
		$res = $this->curlPost('http://localhost/tackapp/public/post',array('name'=>'nama'));
		echo $res;
	}

	function curl($url){
		$URL = $url;
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$URL);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec ($ch);
		curl_close ($ch);
		return $result;
	}

	function matchAll($result, $w1, $w2, $opsi){
		$word1 = $w1;
		$word2 = $w2;
		$ketemu = preg_match_all('/'.preg_quote($word1, '/').'(.*?)'.preg_quote($word2, '/').'/is', $result, $match);
		//0 > lengkap , 1 > hanya yang diapit
		return $match[$opsi]; 
	}

	function match($result, $w1, $w2, $opsi){
		$word1 = $w1;
		$word2 = $w2;
		$ketemu = preg_match('/'.preg_quote($word1, '/').'(.*?)'.preg_quote($word2, '/').'/is', $result, $match);
		if ($ketemu){
		//0 > lengkap , 1 > hanya yang diapit
			return $match[$opsi]; 
		}
		else{
			return '';
		}
	}

	function curlCookie($url){
		$cookie_file_path = dirname(__FILE__)."/cookie.txt";

		// Setup cookie dan stamp value
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		$result = curl_exec ($ch);
		curl_close ($ch);

		return result();
	}

	function curlPost($url,$arrPost){
		$cookie_file_path = dirname(__FILE__)."/cookie.txt";

		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$arrPost);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		$result = curl_exec ($ch);
		curl_close ($ch);

		return $result;
	}

}