<?php
class KaskusLibNew {

	/**
	*
	* search username in kaskus, if exsist return the user id on kaskus
	*
	* @param string $username username in kaskus
	* @return string
	*
	*/
	public static function searchUser($username)
	{
		$url = "http://www.kaskus.co.id/search/username?q=".$username;
		$result = Curl::set()->startCurl($url);

		$delimiter_1 = '<div class="post-title"><a href="/profile/';
		$delimiter_2 = '"';
		$user_id = Curl::set()->match($result, $delimiter_1, $delimiter_2);

		return $user_id;
	}

	/**
	*
	* get list product and it's detail based on user id on kaskus
	*
	* @param string $user_id username in kaskus
	* @return string
	*
	*/
	public static function getProducts($user_id)
	{
		$url = "http://www.kaskus.co.id/profile/viewallclassified/".$user_id;
		$result = Curl::set()->startCurl($url);

		$return_value = array();

		// get product id on kaskus
		$delimiter_1 = '<div class="post-title"><a id="thread_title_';
		$delimiter_2 = '"';
		$product_ids = Curl::set()->matchAll($result, $delimiter_1, $delimiter_2);

		foreach ($product_ids as $product_id) {

			// get poduct url on kaskus to get detail product
			$delimiter_1 = '<div class="post-title"><a id="thread_title_'.$product_id;
			$delimiter_2 = '</div>';
			$temp = Curl::set()->match($result, $delimiter_1, $delimiter_2);

			$delimiter_1 = 'href="';
			$delimiter_2 = '"';
			$product_url = 'www.kaskus.co.id'.Curl::set()->match($temp, $delimiter_1, $delimiter_2);

			// get detail product
			$result_inner = Curl::set()->startCurl($product_url);

			// get product name
			$delimiter_1 = '<h2 class="entry-title" itemprop="name">';
			$delimiter_2 = '</h2>';
			$product_name = Curl::set()->match($result_inner, $delimiter_1, $delimiter_2);

			// get product price
			$delimiter_1 = '<div><span>Harga</span> : Rp. ';
			$delimiter_2 = '</div>';
			$product_price = Curl::set()->match($result_inner, $delimiter_1, $delimiter_2);

			// get product description
			$delimiter_1 = '<div class="product-info">';
			$delimiter_2 = '<footer';
			$temp = Curl::set()->match($result_inner, $delimiter_1, $delimiter_2);

			$delimiter_1 = '</div></div>';
			$delimiter_2 = '</div>';
			$product_desc = strip_tags(Curl::set()->match($temp, $delimiter_1, $delimiter_2));

			// get product images
			$delimiter_1 = '<div class="product-info">';
			$delimiter_2 = '<footer';
			$temp = Curl::set()->match($result_inner, $delimiter_1, $delimiter_2);

			$delimiter_1 = '<img src="';
			$delimiter_2 = '"';
			$product_img = Curl::set()->matchAll($temp, $delimiter_1, $delimiter_2);

			$details = (object) array(
				'id' => $product_id,
				'detail' => (object) array(
					'name' => $product_name,
					'url' => $product_url,
					'price' => $product_price,
					'desc' => $product_desc,
					'images' => $product_img,
					),
				);

			array_push($return_value, $details);
		}
		$return_value = (object) $return_value;
		return $return_value;
	}
}
?>