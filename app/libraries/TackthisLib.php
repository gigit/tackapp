<?php 
class TackthisLib {

	public static function auth()
	{
		$tackthis = new TackthisLib();
		return $tackthis;
	}

	public static function user()
	{
		return Session::get('user_data');
	}

	public static function shopId()
	{
		return Session::get('shop_id');
	}

	/**
	* @return bool
	*/
	public function check()
	{
		// $url = 'http://api.tackthis.com/auth';
		// $result = Curl::set()->startCurl($url);
		// $result = json_decode($result);

		$result = Session::get('user_id');

		if (empty($result)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	* @param string $email
	* @param string $pass
	* @return mixed
	*/
	public function login($email, $pass)
	{
		TackthisLib::auth()->logout();
		$url = 'http://api.tackthis.com/auth/login';

		// get cookie for login by calling url with curl get
		Curl::set()->startCurl($url);

		$fields = array(
			'email' => $email,
			'password' => $pass,
			);

		$result = Curl::set()->startCurl($url, 'post', $fields);
		$result = json_decode($result);

		if (isset($result->error)) {
			return 'false';
		} else {
			//Select or insert from db
			$user = User::firstOrCreate(array('username' => $email));
			Session::set('user_id', $user->id);
			Session::set('user_data', $result);
			Session::set('shop_id', $result->user_cache->last_shop_id);
			return 'true';
		}
	}

	public function logout()
	{
		$url = 'http://api.tackthis.com/auth/logout';
		Curl::set()->startCurl($url);

		Session::forget('shop_id');
		Session::forget('user_id');
		Session::forget('user_data');
		return $this;
	}

	/**
	* @param $data contain post field for post product
	* @return mixed
	*/
	public static function postProduct($data)
	{
		$url = 'http://api.tackthis.com/product';
		
		$product_name = $data['product_name'];
		$description = $data['description'];
		$category = (isset($data['category'])) ? $data['category'] : '' ;
		$images = (isset($data['images'])) ? $data['images'] : '' ; // contain image url in array
		$quantity = (isset($data['quantity'])) ? $data['quantity'] : '0' ;
		$price = $data['price'];

		$fields = array(
			'shop_id' => Session::get('shop_id'),
			'name' => $product_name,
			'description' => $description,
			'category_names' => $category,
			'quantity' => $quantity,
			'price' => $price,
			'images' => json_encode($images),
			);

		$result = Curl::set()->startCurl($url, 'post', $fields);
		return $result;

	}

	public static function getAllProducts()
	{
		$url = 'http://api.tackthis.com/products?shop_id='.Session::get('shop_id');
		$result = Curl::set()->startCurl($url);
		return $result;
	}

	public static function getProduct($id)
	{
		$url = 'http://api.tackthis.com:80/product/'.$id;
		$result = json_decode(Curl::set()->startCurl($url));
		return $result;
	}

}