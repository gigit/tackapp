<?php
class KaskusLib {

	public static function searchUser($username)
	{
		$url = "http://www.kaskus.co.id/search/username?q=".$username;
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$result = curl_exec ($ch);
		curl_close ($ch);

		function getIdUser($str){
			$word1 = '<div class="post-title"><a href="/profile/';
			$word2 = '"';

			$word1 = preg_quote($word1, '/');
			$word2 = preg_quote($word2, '/');
			$match = preg_match('/'.$word1.'(.*?)'.$word2.'/is', $str, $matchItem);
			if ($match) {
				return $matchItem[1];
			} else {
				return '';
			}
		}

		return getIdUser($result);
	}

	public static function getIklan($userId)
	{
		$url = "http://www.kaskus.co.id/profile/viewallclassified/".$userId;
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$result = curl_exec ($ch);
		curl_close ($ch);

		function getListNamaIklan($str){
			$word1 = '<div class="post-title"><a id="thread_title_';
			$word2 = '</a></div>';

			$word1 = preg_quote($word1, '/');
			$word2 = preg_quote($word2, '/');
			$match = preg_match_all('/'.$word1.'(.*?)>(.*?)'.$word2.'/is', $str, $matchItem);
			if ($match) {
				return $matchItem[2];
			} else {
				return '';
			}
		}

		function getListLinkIklan($str){
			$word1 = '<div class="post-title"><a id="thread_title_';
			$word2 = '</a></div>';

			$word1 = preg_quote($word1, '/');
			$word2 = preg_quote($word2, '/');
			$match = preg_match_all('/'.$word1.'(.*?)" href="(.*?)">(.*?)'.$word2.'/is', $str, $matchItem);
			if ($match) {
				return $matchItem[2];
			} else {
				return '';
			}
		}

		$arrJudul = getListNamaIklan($result);
		$arrLink = getListLinkIklan($result);
		$retval = array();

		for ($i=0; $i < count($arrJudul); $i++) { 
			$retval[$i] = array($arrJudul[$i], $arrLink[$i]);
		}

		return $retval;
	}

	public static function getDetailIklan($url)
	{
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$result = curl_exec ($ch);
		curl_close ($ch);

		$namaProduk = self::getNamaProduk($result);
		$harga = self::getHarga($result);
		$deskripsi = self::getDeskripsi($result);
		$gambar = self::getGambar($result);

		$retVal = array(
			'namaProduk' => $namaProduk,
			'harga' => $harga,
			'deskripsi' => $deskripsi,
			'gambar' => $gambar,
			);
		return $retVal;
	}

	public static function getNamaProduk($str){
		$word1 = '<h2 class="entry-title" itemprop="name">';
		$word2 = '</h2>';

		$word1 = preg_quote($word1, '/');
		$word2 = preg_quote($word2, '/');
		$match = preg_match('/'.$word1.'(.*?)'.$word2.'/is', $str, $matchItem);
		if ($match) {
			return $matchItem[1];
		} else {
			return '';
		}
	}

	public static function getHarga($str){
		$word1 = '<div><span>Harga</span> : Rp. ';
		$word2 = '</div>';

		$word1 = preg_quote($word1, '/');
		$word2 = preg_quote($word2, '/');
		$match = preg_match('/'.$word1.'(.*?)'.$word2.'/is', $str, $matchItem);
		if ($match) {
			return str_replace('.', '', $matchItem[1]);
		} else {
			return '';
		}
	}

	public static function getDeskripsi($str){
		$word1 = '<div class="product-info">';
		$word2 = '</div>';

		$word1 = preg_quote($word1, '/');
		$word2 = preg_quote($word2, '/');
		$match = preg_match('/'.$word1.'(.*?)<\/div><\/div>(.*?)'.$word2.'/is', $str, $matchItem);
		if ($match) {
			return strip_tags($matchItem[2]);
		} else {
			return '';
		}
	}

	public static function getGambar($str){
		$word1 = '<div class="product-info">';
		$word2 = '</div>';

		$word1 = preg_quote($word1, '/');
		$word2 = preg_quote($word2, '/');
		$match = preg_match_all('/'.$word1.'(.*?)<\/div><\/div>(.*?)'.$word2.'/is', $str, $matchItem);
		if ($match) {
			$word1 = '<img src="';
			$word2 = '"';

			$word1 = preg_quote($word1, '/');
			$word2 = preg_quote($word2, '/');
			$match2 = preg_match_all('/'.$word1.'(.*?)'.$word2.'/is', $matchItem[2][0], $matchItem2);
			if ($match2) {
				return $matchItem2[1];
			} else {
				return '';
			}
		} else {
			return '';
		}
	}

}

?>