<?php
class Curl {

	private $cookie_file_path;

	public static function set()
	{
		$session_id = Session::getId();
		$curl_config = new Curl();
		$curl_config->cookie_file_path = storage_path().'\cookie'.$session_id.'.txt';
		return $curl_config;
	}

	/**
	*
	* set CURL option and return result of CURL
	*
	* @param string $url
	* @param string $method = 'get' set CURL method get or post, use string 'get' or 'post' to select method.
	* @param array $fields = '' if CURL method set to post, you must provide post field to pass data to the url
	* @param boolean $ssl = '' if CURL url using https set it to true
	* @return mixed
	*
	*/
	public function startCurl($url, $method = 'get', $fields = '', $ssl = false)
	{
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_USERAGENT, $agent);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookie_file_path);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $this->cookie_file_path);

		if ($method == 'post') {
			// curl_setopt($curl, CURLOPT_SAFE_UPLOAD, 0);
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
		}

		if($ssl){
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,  2);
		}

		$result = curl_exec($curl);
		curl_close($curl);
		return $result;
	}

	/**
	*
	* match all string from regex result
	*
	* @param string $words the words will be searched by regex
	* @param string $delimiter_1
	* @param string $delimiter_2
	* @param int $opt = 1 an array index. return value produced based this option. 0 => all sting matches, 1 => only string between delimiter
	* @return string
	*
	*/
	public static function matchAll($words, $delimiter_1, $delimiter_2, $opt = 1)
	{
		$match = preg_match_all('/'.preg_quote($delimiter_1, '/').'(.*?)'.preg_quote($delimiter_2, '/').'/is', $words, $match_item);
		if ($match) {
			return $match_item[$opt]; 
		} else {
			return '';
		}
	}

	/**
	*
	* match first string from regex result
	*
	* @param string $words the words will be searched by regex
	* @param string $delimiter_1
	* @param string $delimiter_2
	* @param int $opt = 1 an array index. return value produced based this option. 0 => all sting matches, 1 => only string between delimiter
	* @return string
	*
	*/
	public static function match($words, $delimiter_1, $delimiter_2, $opt = 1)
	{
		$match = preg_match('/'.preg_quote($delimiter_1, '/').'(.*?)'.preg_quote($delimiter_2, '/').'/is', $words, $match_item);
		if ($match) {
			return $match_item[$opt]; 
		} else {
			return '';
		}
	}
}
?>