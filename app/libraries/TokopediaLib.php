<?php 
class OlxLib {

	public static function auth()
	{
		$olx = new OlxLib();
		return $olx;
	}

	/**
	*
	* login to olx.co.id and return username
	*
	* @param string $email
	* @param string $pass
	* @return string
	*/
	public function login($email, $pass)
	{
		$url = 'http://www.olx.co.id/login';
		
		// get cookie for login by calling url with curl get
		$result = Curl::set()->startCurl($url);

		// get stamp value
		$stamp = Curl::set()->match($result, '<input name="stamp" type="hidden" value="', '"');

		$fields = array(
			'email' => $email,
			'password' => $pass,
			'stamp' => $stamp,
			'submit' => 'login',
			);

		$result = Curl::set()->startCurl($url, 'post', $fields);
		
		$user_id = Curl::set()->match($result, '<small class="grey">Hi, </small><strong>', '</strong>');
		Session::set('olx_user_id', $user_id);
		return $user_id;
	}

	/**
	* @param $product_id contain post field for post product
	* @return mixed
	*/
	public static function postProduct($product_id)
	{
		// get images from tackthis
		$olx_images = array();
		$olx_img_url = '';
		$detail_product = TackthisLib::getProduct($product_id);
		foreach ($detail_product->images as $key => $image) {
			$img_url = $image->url;
			$img_path = self::downloadImage($img_url);
			$olx_img_url = self::getTempImageUrl($img_path);
			array_push($olx_images, $olx_img_url);
		}

		// get stamp for submit form
		$url = 'http://www.olx.co.id/pasang';
		$result = Curl::set()->startCurl($url);

		$user_id = Curl::set()->match($result, '<input type="hidden" name="userid" id="userid" value="', '"');
		$stamp = Curl::set()->match($result, '<input type="hidden" name="stamp" id="stamp" value="', '"');

		$fields = array(
			'AUC_TYPE2' => '1',
			'AUC_TITLE' => $detail_product->name,
			'AUC_DESCR' => $detail_product->description,
			'AUC_AD_PRICE' => $detail_product->price,
			'AUC_CONDITION' => 'baru',
			'AUC_LOCATION' => '32',
			'extra[p_kab]' => '70',
			'photo_img[0][url]' => $olx_img_url,
			'bjb_detail[percentage]' => '50',
			'bjb_detail[foundation_code]' => '',
			'bjb_detail[foundation_text]' => 'Silakan Pilih',
			'bjb_detail[description]' => '',
			'chk_terms_agree' => '1',
			'userid' => $user_id,
			'AUC_SECTION' => '92',
			'AUC_SUBSECTION' => '215',
			'AUC_CAT' => '4950',
			'stamp' => $stamp,
			'q' => 'Kata Pencarian...',
			);

		$result = Curl::set()->startCurl($url, 'post', $fields);
		return $result;
	}

	public static function downloadImage($img_url)
	{
		$path_parts = pathinfo($img_url);
		$img = $path_parts['basename'];
		$img_path = storage_path().'\temp\olx\\'.$img;

		file_put_contents($img_path, file_get_contents($img_url));
		return $img_path;
	}

	public static function getTempImageUrl($img_path)
	{
		$img_path = curl_file_create($img_path, 'image/png', 'test.png');

		$url = 'http://www.olx.co.id/pasang/ajax-upload';

		// dump data to get temp image url from olx site
		$fields = array(
			'file' => $img_path,
			'uid' => 'temp',
			);
		$result = Curl::set()->startCurl($url, 'post', $fields);
		$result = json_decode($result);
		$temp_img_url = $result->res->src;
		// echo $temp_img_url;
		// die();
		return $temp_img_url;
	}
}