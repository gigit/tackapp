<?php 
class OlxLib {

	public static function auth()
	{
		$olx = new OlxLib();
		return $olx;
	}

	/**
	*
	* login to olx.co.id and return username
	*
	* @param string $email
	* @param string $pass
	* @return string
	*/
	public function login($email, $pass)
	{
		$url = 'http://www.olx.co.id/login';
		
		// get cookie for login by calling url with curl get
		$result = Curl::set()->startCurl($url);

		// get stamp value
		$stamp = Curl::set()->match($result, '<input name="stamp" type="hidden" value="', '"');

		$fields = array(
			'email' => $email,
			'password' => $pass,
			'stamp' => $stamp,
			'submit' => 'login',
			);

		$result = Curl::set()->startCurl($url, 'post', $fields);
		$user_id = "";
		$user_id = Curl::set()->match($result, '<small class="grey">Hi, </small><strong>', '</strong>');

		if (empty($user_id)) {
			return 3; //errno 3 login failed
		}else{
			Session::set('olx_user_id', $user_id);
			return $user_id;
		}
	}

	/**
	* @param $product_id contain post field for post product
	* @return mixed
	*/
	public static function postProduct($product_id)
	{
		// get images from tackthis
		$olx_images = array();
		$temp_img_local_path = array();
		$olx_img_url = '';
		$detail_product = TackthisLib::getProduct($product_id);
		foreach ($detail_product->images as $key => $image) {
			$img_url = $image->url;
			$img_local_path = self::downloadImage($img_url);
			$olx_img_url = self::getTempImageUrl($img_local_path);
			array_push($olx_images, $olx_img_url);
			array_push($temp_img_local_path, $img_local_path);
		}

		// get stamp for submit form
		$url = 'http://www.olx.co.id/pasang';
		$result = Curl::set()->startCurl($url);

		$user_id = Curl::set()->match($result, '<input type="hidden" name="userid" id="userid" value="', '"');
		$stamp = Curl::set()->match($result, '<input type="hidden" name="stamp" id="stamp" value="', '"');

		$fields = array(
			'AUC_TYPE2' => '1',
			'AUC_TITLE' => $detail_product->name,
			'AUC_DESCR' => $detail_product->description,
			'AUC_AD_PRICE' => $detail_product->price,
			'AUC_CONDITION' => 'baru',
			'AUC_LOCATION' => '32',
			'extra[p_kab]' => '70',
			'photo_img[0][url]' => $olx_img_url,
			'bjb_detail[percentage]' => '50',
			'bjb_detail[foundation_code]' => '',
			'bjb_detail[foundation_text]' => 'Silakan Pilih',
			'bjb_detail[description]' => '',
			'chk_terms_agree' => '1',
			'userid' => $user_id,
			'AUC_SECTION' => '92',
			'AUC_SUBSECTION' => '215',
			'AUC_CAT' => '4950',
			'stamp' => $stamp,
			'q' => 'Kata Pencarian...',
			);

		$result = Curl::set()->startCurl($url, 'post', $fields);

		foreach ($temp_img_local_path as $key => $value) {
			unlink($value);
		}
		return $result;
	}

	public static function downloadImage($img_url)
	{
		$path_parts = pathinfo($img_url);
		$img = $path_parts['basename'];
		$img_local_path = storage_path().'\temp\olx\\'.$img;

		file_put_contents($img_local_path, file_get_contents($img_url));
		return $img_local_path;
	}

	public static function getTempImageUrl($img_local_path)
	{
		echo $img_local_path;

		if (!function_exists('curl_file_create')) {
			function curl_file_create($filename, $mimetype = '', $postname = '') {
				return "@$filename;filename="
				. ($postname ?: basename($img_local_path))
				. ($mimetype ? ";type=$mimetype" : '');
			}
		}

		$img_path = curl_file_create($img_local_path, 'image/png', 'test.png');

		$url = 'http://www.olx.co.id/pasang/ajax-upload';

		// dump data to get temp image url from olx site
		$fields = array(
			'file' => $img_path,
			'uid' => 'temp',
			);
		$result = Curl::set()->startCurl($url, 'post', $fields);
		$result = json_decode($result);
		$temp_img_url = $result->res->src;
		return $temp_img_url;
	}

	public static function getProducts($olx_username)
	{
		$product = array();
		$url = 'http://m.olx.co.id/users/'.$olx_username;
		$curl = Curl::set()->startCurl($url);
		$names 	= Curl::set()->matchAll($curl,'<a href="http://m.olx.co.id/iklan/','</a>',0);
		$urls 	= Curl::set()->matchAll($curl,'<a href="http://m.olx.co.id/iklan/','"',1);

		//do loop per product
		//init variable
		$photos = array();
		$prices  = array();
		$descriptions = array();
		$realNames= array();
		$productNumb = 0;
		if(empty($urls)){
			return '';
		}
		foreach ($urls as $key => $value) {
			//get photos
			$photos[$productNumb] = array();
			$curl = Curl::set()->startCurl('http://m.olx.co.id/iklan/'.$value);
			$photos[$productNumb] = Curl::set()->matchAll($curl,'<img class="ps-carousel-item" src="','"',1);

			//get price
			$outerPrice = Curl::set()->match($curl,'<div class="price">','</div>',1);
			$price 		= Curl::set()->match($outerPrice,'<span>','</span>',1);
			$price 		= str_replace('.', '',$price);
			$prices[$productNumb]		= $price;

			//get description
			$descriptions[$productNumb] = Curl::set()->match($curl, '<div class="addesc">','</div>',1);

			//get names
			$realName = Curl::set()->match($curl,'<div class="adtitle">','</div>',1);
			$realName = Curl::set()->match($realName,'<h1>','</h1>',1);
			$realName = str_replace('<span>Dijual</span>', '',$realName);

			$realNames[$productNumb]	= $realName;

			$productNumb++;
		}

		$product['name']		= $names;
		$product['url']			= $urls;
		$product['price']		= $prices;
		$product['photo']		= $photos;
		$product['description']	= $descriptions;
		$product['realname']	= $realNames;

		return $product;
	}
}