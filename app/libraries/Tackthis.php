<?php
class Tackthis {

	public static function auth()
	{
		$cookie_file_path = dirname(__FILE__)."/cookie.txt";
		$url = 'http://api.tackthis.com/auth';
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";

		$ch = curl_init();

		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($ch);

		$result = json_decode($result);
		curl_close($ch);
		if (isset($result->error)) {
			return false;
		} else {
			return $result;
		}
	}

	public static function login($email, $pass)
	{
		$cookie_file_path = dirname(__FILE__)."/cookie.txt";
		$url = 'http://api.tackthis.com/auth/login';
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		$result = curl_exec ($ch);
		curl_close ($ch);

		$fields = array(
			'email' => urlencode($email),
			'password' => urlencode($pass),
			);

		$fields_string = '';
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		$ch = curl_init();

		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_USERAGENT, $agent);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($ch);

		curl_close($ch);

		$result = json_decode($result);
		Session::put('idToko',$result->user_cache->last_shop_id);
		return $result;
	}

	public static function logout()
	{
		$cookie_file_path = dirname(__FILE__)."/cookie.txt";
		$url = 'http://api.tackthis.com/auth/logout';
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";

		$ch = curl_init();

		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($ch);

		curl_close($ch);

		Session::forget('idToko');
		return $result;
	}

	public static function postProduk($data)
	{
		$cookie_file_path = dirname(__FILE__)."/cookie.txt";
		$url = 'http://api.tackthis.com/product';
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
		
		$namaProduk = $data['namaProduk'];
		$deskripsi = $data['deskripsi'];
		$kategori = (isset($data['kategori'])) ? $data['kategori'] : '' ;
		$gambar = (isset($data['gambar'])) ? $data['gambar'] : '' ; //isinya array url gambar
		$kuantitas = (isset($data['kuantitas'])) ? $data['kuantitas'] : '0' ;
		$harga = $data['harga'];

		$fields = array(
			'shop_id' => urlencode(Session::get('idToko')),
			'name' => urlencode($namaProduk),
			'description' => urlencode($deskripsi),
			'category_names' => urlencode($kategori),
			'quantity' => urlencode($kuantitas),
			'price' => urlencode($harga),
			'images' => json_encode($gambar),
			);

		$fields_string = '';
		foreach($fields as $key => $value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		$ch = curl_init();

		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($ch);

		curl_close($ch);
		return $result;

	}

	public static function getAllProducts()
	{
		$cookie_file_path = dirname(__FILE__)."/cookie.txt";
		$url = 'http://api.tackthis.com/products?shop_id='.Session::get('idToko');
		$agent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";

		$ch = curl_init();

		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($ch);

		curl_close($ch);
		return $result;
	}


}

?>