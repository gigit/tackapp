<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::when('/admin/*', 'admin');

// Route::get('/post',function(){
// 	$result 	= Curl::set()->startCurl('https://','get','',true);
// 	echo $result;
// });

Route::group(array('before' => 'auth_tackthis'), function()
{
	Route::controller('/kaskus', 'KaskusController');
	Route::controller('/olx', 'OlxController');
});
	// Route::controller('/test', 'TestController');
	Route::controller('/tokopedia', 'TokopediaController');

Route::controller('/', 'HomeController');