<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserToRedeemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('user_redeem_code', function($table){
			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->bigInteger('user_id')->unsigned();
			$table->bigInteger('redeem_code_id')->unsigned();
			$table->timestamps();

			$table->foreign('user_id')
				->references('id')->on('user')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->foreign('redeem_code_id')
				->references('id')->on('redeem_code')
				->onDelete('cascade')
				->onUpdate('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('user_redeem_code');
	}

}
