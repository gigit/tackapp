<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('user', function($table){
			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->string('username', 128)->unique();
			$table->string('email', 128)->unique();
			$table->string('password', 256);
			$table->boolean('pengaktifan')->default(false);
			$table->string('token_pengaktifan', 256);
			$table->string('token_pengingat', 256)->nullable();
			$table->timestamps();
			//$table->tinyInteger('group_id')->unsigned();
			$table->string('group', 128);

			$table->foreign('group')
				->references('nama')->on('group')
				->onDelete('restrict')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('user');
	}

}
