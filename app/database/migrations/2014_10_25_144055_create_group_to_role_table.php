<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupToRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('group_role',function($table)
		{
			$table->engine = 'InnoDB';
			
			//$table->bigIncrements('id')->unsigned();
			$table->string('group', 128);
			$table->string('role', 128);

			$table->foreign('group')
				->references('nama')->on('group')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->foreign('role')
				->references('nama')->on('role')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('group_role');
	}

}
