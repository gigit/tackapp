<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTantanganTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('tantangan', function($table){
			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->string('nama', 128);
			$table->text('deskripsi')->nullable();
			$table->integer('skor_exp');
			$table->integer('skor_poin')->default(0);
			$table->text('img')->nullable();
			$table->timestamp('tgl_mulai');
			$table->timestamp('tgl_akhir');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('tantangan');
	}

}
