<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('periode',function($table)
		{
			$table->engine = 'InnoDB';
			
			//$table->bigIncrements('id')->unsigned();
			$table->string('nama', 128);
			$table->text('deskripsi');

			$table->primary('nama');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('periode');
	}

}
