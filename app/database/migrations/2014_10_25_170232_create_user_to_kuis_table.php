<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserToKuisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('user_kuis', function($table){
			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->text('jawaban')->nullable();
			$table->text('skor')->nullable();
			$table->bigInteger('user_id')->unsigned();
			$table->bigInteger('kuis_id')->unsigned();

			$table->foreign('user_id')
				->references('id')->on('user')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->foreign('kuis_id')
				->references('id')->on('kuis')
				->onDelete('cascade')
				->onUpdate('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('user_kuis');
	}

}
