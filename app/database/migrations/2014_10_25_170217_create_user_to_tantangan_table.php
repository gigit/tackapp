<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserToTantanganTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('user_tantangan', function($table){
			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->bigInteger('user_id')->unsigned();
			$table->bigInteger('tantangan_id')->unsigned();

			$table->foreign('user_id')
				->references('id')->on('user')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->foreign('tantangan_id')
				->references('id')->on('tantangan')
				->onDelete('cascade')
				->onUpdate('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('user_tantangan');
	}

}
