<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserToLencanaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('user_lencana', function($table){
			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->boolean('status')->default(false);
			$table->integer('skor');
			$table->timestamps();
			$table->bigInteger('user_id')->unsigned();
			$table->bigInteger('lencana_id')->unsigned();

			$table->foreign('user_id')
				->references('id')->on('user')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->foreign('lencana_id')
				->references('id')->on('lencana')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('user_lencana');
	}

}
