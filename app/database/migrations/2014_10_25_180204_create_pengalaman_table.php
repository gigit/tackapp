<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengalamanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('pengalaman', function($table){
			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->integer('exp');
			$table->integer('poin')->default(0);
			//$table->bigInteger('periode_id')->unsigned();
			$table->string('periode', 128);
			$table->bigInteger('user_id')->unsigned();

			$table->foreign('periode')
				->references('nama')->on('periode')
				->onDelete('restrict')
				->onUpdate('cascade');

			$table->foreign('user_id')
				->references('id')->on('user')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
