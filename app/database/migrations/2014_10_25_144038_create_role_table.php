<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('role',function($table)
		{
			$table->engine = 'InnoDB';
			
			//$table->tinyInteger('id')->unsigned()->autoIncrement();
			$table->string('nama', 128)->unique();
			$table->text('deskripsi');

			$table->primary('nama');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('role');
	}

}
