<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('prodi',function($table)
		{
			$table->engine = 'InnoDB';
			
			$table->bigIncrements('id')->unsigned();
			$table->string('nama', 128)->unique();
			$table->bigInteger('jurusan_id')->unsigned();

			$table->foreign('jurusan_id')
		    	->references('id')->on('jurusan')
		    	->onDelete('restrict')
		      	->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('prodi');
	}

}
