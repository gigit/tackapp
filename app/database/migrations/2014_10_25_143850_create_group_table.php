<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('group',function($table)
		{
			$table->engine = 'InnoDB';
			
			//$table->tinyInteger('id')->unsigned()->autoIncrement();
			$table->string('nama', 128);
			$table->text('deskripsi');

			$table->primary('nama');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('group');
	}

}
