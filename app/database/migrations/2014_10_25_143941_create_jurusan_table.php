<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJurusanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('jurusan',function($table)
		{
			$table->engine = 'InnoDB';
			
			$table->bigIncrements('id')->unsigned();
			$table->string('nama', 128)->unique();
			$table->bigInteger('fakultas_id')->unsigned();

			$table->foreign('fakultas_id')
		    	->references('id')->on('fakultas')
		    	->onDelete('restrict')
		      	->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('jurusan');
	}
}
