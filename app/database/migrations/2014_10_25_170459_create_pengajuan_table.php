<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('pengajuan', function($table){
			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->integer('skor')->nullable();
			$table->timestamps();
			$table->bigInteger('tantangan_id')->unsigned();
			$table->bigInteger('penghargaan_id')->unsigned();

			$table->foreign('penghargaan_id')
				->references('id')->on('penghargaan')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->foreign('tantangan_id')
				->references('id')->on('tantangan')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('pengajuan');
	}

}
