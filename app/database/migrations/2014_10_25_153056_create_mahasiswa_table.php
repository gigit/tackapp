<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('mahasiswa', function($table){
			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->string('nama_depan', 128);
			$table->string('nama_blkg', 128);
			$table->text('address');
			$table->string('telp', 128)->nullable();
			$table->text('foto');
			$table->boolean('lulus')->default(false);
			$table->timestamps();
			$table->bigInteger('prodi_id')->unsigned();
			$table->bigInteger('user_id')->unsigned()->unique();

			$table->foreign('prodi_id')
				->references('id')->on('prodi')
				->onDelete('restrict')
				->onUpdate('cascade');

			$table->foreign('user_id')
				->references('id')->on('user')
				->onDelete('restrict')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('mahasiswa');
	}

}
