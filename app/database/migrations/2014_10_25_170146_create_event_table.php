<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('event', function($table){
			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->string('nama', 128);
			$table->text('deskripsi')->nullable();
			$table->text('img')->nullable();
			$table->timestamp('tgl_mulai');
			$table->timestamp('tgl_akhir');
			$table->timestamps();
			//$table->bigInteger('periode_id')->unsigned();
			$table->string('periode', 128);

			$table->foreign('periode')
				->references('nama')->on('periode')
				->onDelete('restrict')
				->onUpdate('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('event');
	}

}
