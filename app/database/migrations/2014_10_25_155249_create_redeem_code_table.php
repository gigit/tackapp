<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedeemCodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('redeem_code', function($table){
			$table->engine = 'InnoDB';

			$table->bigIncrements('id')->unsigned();
			$table->string('kode', 256);
			$table->integer('bonus_exp');
			$table->integer('bonus_point');
			$table->boolean('redeemed')->default(false);
			$table->timestamp('expired_at');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('redeem_code');
	}

}
