## Specs & Requirements
- Framework Laravel 4.2
- Minimum PHP 5.5
- Database MYSQL 
- Using curl php *please enable it if not enabled
- enable openssl on php.ini *may be required

Note : For local environment need the internet connection, if some error occured try to reload your browser or check your connection *

## How to install
1. Clone this repo
2. Do "composer install" on the root app dir to install laravel *need to install composer
3. Import MySQL database tackapp.sql *included in repo
4. Setup database config in app/config/database.php 
5. try to access http://{yourhost}/public
6. login with tackthis account
7. Landing to the dashboard

Note : you can also try from here http://tackapp.monsterkode.com/public/

##Current version function
- Import : olx, kaskus, tokopedia
- Export : olx



Developed by Craterio Team